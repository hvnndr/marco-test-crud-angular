from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait


class BasePage:

    def __init__(self, driver):
        self.driver = driver
        self.wait = WebDriverWait(self.driver, 10)


class UserPage(BasePage):

    url = "https://images.unsplash.com/photo-1508921912186-1d1a45ebb3c1?ixlib=r" \
          "b-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8cGhvdG98ZW58MHx8MHx8&w=1000&q=80"

    def ClickButtonEditName(self):
        self.driver.find_element(By.XPATH, "//div[@class='btn-group']/button[1]").click()

    def EditName(self, name):
        self.ClickButtonEditName()
        self.driver.find_element(By.ID, "nome").clear()
        self.driver.find_element(By.ID, "nome").send_keys(name)
        self.driver.find_element(By.XPATH, "//button[@class='btn btn-primary']").click()

    def CadastrarVariasPessoas(self):
        pessoas = ['Joao', 'José', 'Maria', 'Mario', 'Alan']
        for n in pessoas:
            self.CadastroPessoa(n, f"{n.lower()}@fpf.br", self.url)

    def CadastroPessoa(self, nome, email, url=None):
        self.driver.find_element(By.ID, "nome").send_keys(nome)
        self.driver.find_element(By.ID, "exampleInputEmail1").send_keys(email)
        self.driver.find_element(By.NAME, "photo").send_keys(self.url)
        self.driver.find_element(By.XPATH, "//button[@class='btn btn-primary']").click()

    def GetName(self, name):
        return self.driver.find_element(By.XPATH, "//div[@class='card']//p[contains(. , '" + name + "')]").text


class MessagePage(BasePage):

    def ReturnMessage(self):
        return self.wait.until(EC.visibility_of_element_located((By.XPATH, "//div[@role='alertdialog']"))).text
