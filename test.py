from selenium import webdriver
import unittest
import xmlrunner
from pages import UserPage, MessagePage
from selenium.webdriver.chrome.options import Options


class BaseTest(unittest.TestCase):

    def setUp(self):


        chrome_options = Options()
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.driver = webdriver.Chrome('/usr/bin/chromedriver',chrome_options=chrome_options)
        self.driver.maximize_window()
        self.driver.get("https://vanilton18.github.io/crud-local-storage-angular/")
        self.user_page = UserPage(driver=self.driver)
        self.message_page = MessagePage(driver=self.driver)

    def tearDown(self):
        self.driver.quit()



class UserTest(BaseTest):

#Pre
#Test
#Validacao

    def test_cadastro_valido(self):
        self.user_page.CadastroPessoa(nome="Evaldo", email="evaldo.santos@fpf.br")
        self.message = 'Cadastro efetuado com sucesso!'
        self.assertEquals(self.message, self.message_page.ReturnMessage())

    def test_cadastro_sem_nome(self):
        self.user_page.CadastroPessoa(nome="", email="evaldo.santos@fpf.br")
        self.message = 'Cadastro efetuado com sucesso!'
        self.assertNotEqual(self.message, self.message_page.ReturnMessage())

    def test_cadastro_sem_email(self):
        self.user_page.CadastroPessoa(nome="Evaldo", email="")
        self.message = 'Cadastro efetuado com sucesso!'
        self.assertNotEquals(self.message, self.message_page.ReturnMessage())

    def test_alterar_pessoa(self):
        self.user_page.CadastroPessoa(nome="José da Silva", email="jose.silva@fpf.br")
        self.user_page.EditName("Fulano Da Silva")
        name = 'Nome: Fulano Da Silva'
        self.assertEquals(name, self.user_page.GetName(name))

    def test_cadastrar_email_existente(self):
        self.user_page.CadastroPessoa('evaldo', 'evaldo.santos@fpf.br')
        self.user_page.CadastroPessoa('Joao', 'evaldo.santos@fpf.br')
        self.message = 'Cadastro efetuado com sucesso!'
        self.assertNotEquals(self.message, self.message_page.ReturnMessage())

if __name__ == '__main__':
    with open('/path/to/results.xml', 'wb') as output:
        unittest.main(
            testRunner=xmlrunner.XMLTestRunner(output=output),
            failfast=False, buffer=False, catchbreak=False)
